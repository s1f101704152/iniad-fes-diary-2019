from django.db import models
from django_boost.models.mixins import UUIDModelMixin,TimeStampModelMixin

# Create your models here.

class Diary(UUIDModelMixin,TimeStampModelMixin):
    title = models.CharField(max_length=128)
    text = models.TextField()
