const diary_wrapper = document.getElementById("diary_wrapper");
const header = document.getElementsByTagName("header")[0];
diary_wrapper.onscroll = function () {
    if (32 <= diary_wrapper.scrollTop) {
        header.classList.add("header-shadow");
    } else {
        header.classList.remove("header-shadow");
    }
}