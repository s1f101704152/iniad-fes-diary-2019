from django.urls import path
from . import views

app_name = 'diary'


urlpatterns = [
    path('', views.DiaryListView.as_view(),name='list'),
    path('create/', views.DiaryCreateView.as_view(), name='create'),
    path('<uuid:pk>/', views.DiaryDetailView.as_view(),name='detail'),
    path('<uuid:pk>/update/', views.DiaryUpdateView.as_view(),name='update'),
    path('<uuid:pk>/delete/', views.DiaryDeleteView.as_view(),name='delete'),
]
