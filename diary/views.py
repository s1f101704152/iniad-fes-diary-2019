from django.views.generic import *
from django.urls import reverse_lazy

from .models import Diary
from .forms import DiaryForm


class DiaryListView(ListView):
    model = Diary


class DiaryDetailView(DetailView):
    model = Diary


class DiaryUpdateView(UpdateView):
    model = Diary
    form_class = DiaryForm
    success_url = reverse_lazy('diary:list')


class DiaryCreateView(CreateView):
    model = Diary
    form_class = DiaryForm
    success_url = reverse_lazy('diary:list')


class DiaryDeleteView(DeleteView):
    model = Diary
    success_url = reverse_lazy('diary:list')
